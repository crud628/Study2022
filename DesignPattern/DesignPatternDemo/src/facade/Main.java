package facade;

import facade.pageMaker.PageMaker;

/**
 * @author Keason
 * @version 创建时间：2022年11月21日 下午10:10:49
 * @TODO
 * 
 */
public class Main {
    public static void main(String[] args) {
        PageMaker.makeWelcomePage("hyuki@hyuki.com", "welcome.html");
    }
}
