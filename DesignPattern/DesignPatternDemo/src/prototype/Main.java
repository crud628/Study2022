package prototype;

import prototype.factory.Manager;
import prototype.factory.Product;

/**
 * @author Keason
 * @version 创建时间：2022年11月13日 下午9:13:02
 * @TODO
 * 
 */
public class Main {
	public static void main(String[] args) {
		// 准备
		Manager manager = new Manager();
		UnderlinePen upen = new UnderlinePen('~');
		MessageBox mbox = new MessageBox('*');
		MessageBox sbox = new MessageBox('/');
		manager.register("strong message", upen);
		manager.register("warning box", mbox);
		manager.register("slash box", sbox);

		// 生成
		Product p1 = manager.create("strong message");
		p1.use("Hello, world.");
		Product p2 = manager.create("warning box");
		p2.use("Hello, world.");
		Product p3 = manager.create("slash box");
		p3.use("Hello, world.");
	}
}
