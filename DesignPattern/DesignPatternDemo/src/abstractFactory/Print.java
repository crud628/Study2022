package abstractFactory;

/**
 * @author Keason
 * @version 创建时间：2022年11月19日 下午9:27:07
 * @TODO
 * 
 */
public interface Print {
	public abstract void printWeak();

	public abstract void printStrong();
}
