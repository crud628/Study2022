package bridge;

/**
 * @author Keason
 * @version 创建时间：2022年11月19日 下午9:30:33
 * @TODO
 * 
 */
public abstract class DisplayImpl {
	public abstract void rawOpen();

	public abstract void rawPrint();

	public abstract void rawClose();
}
