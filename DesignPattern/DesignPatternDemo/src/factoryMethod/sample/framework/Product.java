package factoryMethod.sample.framework;
/**
 * @author Keason
 * @version 创建时间：2022年11月13日 下午8:35:55
 * @TODO
 * 
 */
public abstract class Product {
	
	public abstract void use();
}
