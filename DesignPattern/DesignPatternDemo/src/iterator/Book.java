package iterator;
/**
 * @author Keason
 * @version 创建时间：2022年11月1日 下午10:20:45
 * @TODO 书 测试类
 * 
 */
public class Book {
	/**
	 * 书名
	 */
    private String name;
    
    public Book(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
