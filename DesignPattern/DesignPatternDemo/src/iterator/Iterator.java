package iterator;
/**
 * @author Keason
 * @version 创建时间：2022年11月1日 下午10:20:12
 * @TODO 
 * 
 */
public interface Iterator {
	
    public abstract boolean hasNext();
    
    public abstract Object next();
}
