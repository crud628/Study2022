package iterator;
/**
 * @author Keason
 * @version 创建时间：2022年11月1日 下午10:19:51
 * @TODO 集合接口
 * 
 */
public interface Aggregate {
	public abstract Iterator iterator();
}
