package com.lan.springframework.context;

import com.lan.springframework.beans.factory.ListableBeanFactory;

/**
 * @author Keason
 * @version 创建时间：2022年11月20日 下午7:55:23
 * @TODO
 * 
 */
public interface ApplicationContext extends ListableBeanFactory {

}
