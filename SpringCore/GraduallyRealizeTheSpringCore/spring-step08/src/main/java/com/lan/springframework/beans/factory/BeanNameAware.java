package com.lan.springframework.beans.factory;

/**
 * @author Keason
 * @version 创建时间：2022年11月20日 下午12:20:46
 * @TODO
 * 
 */
public interface BeanNameAware {
	void setBeanName(String name);
}
