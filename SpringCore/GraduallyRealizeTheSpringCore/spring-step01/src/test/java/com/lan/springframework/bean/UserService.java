package com.lan.springframework.bean;
/**
 * @author Keason
 * @version 创建时间：2022年11月12日 下午1:23:05
 * @TODO 对象类，方便测试
 * @since 0.01
 */
public class UserService {
	/**
	 * 对象的方法
	 */
    public void queryUserInfo(){
        System.out.println("查询用户信息");
    }
}
