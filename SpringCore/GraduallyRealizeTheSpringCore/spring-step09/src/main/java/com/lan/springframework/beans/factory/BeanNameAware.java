package com.lan.springframework.beans.factory;
/**
 * @author Keason
 * @version 创建时间：2022年11月21日 下午9:30:18
 * @TODO
 * 
 */
public interface BeanNameAware {

    void setBeanName(String name);

}
