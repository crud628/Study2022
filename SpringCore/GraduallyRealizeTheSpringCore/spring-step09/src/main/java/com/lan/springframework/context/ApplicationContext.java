package com.lan.springframework.context;

import com.lan.springframework.beans.factory.ListableBeanFactory;

/**
 * @author Keason
 * @version 创建时间：2022年11月21日 下午9:50:34
 * @TODO
 * 
 */
public interface ApplicationContext extends ListableBeanFactory {
}
