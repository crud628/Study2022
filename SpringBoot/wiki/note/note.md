# Spring Boot+Vue3前后端分离，实战wiki知识库系统

### 第五章

集成HTTP库axios

 完成电子书列表

集成axios

Vue3新增了setup初始化方法

跨域 可以这样理解，来自一个IP端口的页面（vue项目），要访问另一个IP端口的资源 （springboot请求接口），会产生跨域访问。 增加CorsConfig配置类，解决跨域问题。



接口前后端交互

电子书列表界面展示

axios拦截器的使用

Vue CLI多环境配置

使用过滤器记录接口耗时

使用拦截器记录接口耗时

使用AOP记录接口耗时

使用Validation做参数校验
