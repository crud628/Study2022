package com.lan.mybilibili.domain.constant;

public class UserConstant {

    /**
     * 性别 男
     */
    public static final String GENDER_MALE = "0";

    /**
     * 性别 女
     */
    public static final String GENDER_FEMALE = "1";

    /**
     * 性别 位置
     */
    public static final String GENDER_UNKNOW = "0";

    /**
     * 默认出生日期
     */
    public static final String DEFAULT_BIRTH = "1999-10-01";

    /**
     * 默认昵称
     */
    public static final String DEFAULT_NICK = "萌新";

    public static final String USER_FOLLOWING_GROUP_TYPE_DEFAULT = "2";

    public static final String USER_FOLLOWING_GROUP_TYPE_USER = "3";

    public static final String USER_FOLLOWING_GROUP_ALL_NAME = "全部关注";
}
