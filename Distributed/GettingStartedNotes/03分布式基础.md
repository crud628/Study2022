# 分布式基础

## 一.  权威定义

利用物理架构形成多个自治的处理元素，不共享主内存，但是通过发送信息合作。

## 二.  作用

技术的诞生，是为了解决问题

### 单体应用的问题

- 速度变慢

- 熟悉项目的工作量太大
- 耦合严重
- 合并代码冲突多，依赖冲突多

### 分布式的好处

- 开发、部署的速度都更快
- 技术升级的空间大
- 可用性增强
- 成本低
- 资源利用率提高

## 三.  分布式与单体结构的对比

![image-20220815224431041](https://typora-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220815224431041.png)

![image-20220815224557360](https://typora-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220815224557360.png)



## 四.  CAP定理

### 1.  定义

- C（Consistency，一致性）：总能读到最新的写操作的结果
- A（Availability，可用性）：每个请求都要在合理的时间内给出响应  | 数据可能不是最新的
- P（Partion tolerance，分区容错性）：当节点之间的网络不通，系统能够继续运行

三者不可兼得

<img src="https://typora-imagebed.oss-cn-beijing.aliyuncs.com/img/image-20220815225156010.png" alt="image-20220815225156010" style="zoom:67%;" />

### 2.  CAP如何选

P一定存在，CP    or   AP

例子

CDN：图片不一定是最新的AP

转账：强一致性，允许不可用，但必须一致CP



## 五.  集群、分布式、微服务的区别

分布式：不同的项目。部署在多个服务器上

集群：同一个项目，部署在多个服务器上，分散压力

微服务：分散能力，是架构的设计方式，按业务垂直拆分