package com.lan.course_list;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Keason
 * @version 创建时间：2022年10月22日 下午2:43:15
 * @TODO
 * 
 */
@SpringBootApplication
public class CourseListApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(CourseListApplication.class, args);
	}
}
