# 《Java并发编程实战》代码梳理

> 原英文版《Java Concurrency in Practice》所以工程取名JCIP



目录说明

```
│├─lib                            项目所需jar包
│      junit.jar      
│      servlet.jar
│
└─src
    └─com
        └─lan
            └─test
                ├─annotations     自定义注解
                ├─brief           简介
                ├─common          共通包，由于分包之后权限限制，所以单独存放
                ├─part01          第一部分：基础篇，2-5章
                ├─part02          第二部分：结构化并发应用程序，6-9章         
                ├─part03          第三部分：活跃性，性能与测试，10-12章
                └─part04          第四部分：高级主题，13-16章
```

